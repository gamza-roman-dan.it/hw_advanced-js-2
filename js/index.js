"use strict"

/*
це для перехоплення помилок, наприклад коли підключається третя сторона й ми не впевненні у коді, чи код з серверу,
або для роботи з такими масивами "як у цій дз." у яких ми не впевнені.
*/

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


const bookContainer = document.querySelector("#root");


class RenderBook {
    constructor(author, name, price) {
        this.author = author;
        this.name = name;
        this.price = price;
    };

    render(container){
        container.insertAdjacentHTML("beforeend", `
            <ul>
                <li>autor:  ${this.author}</li>
                <li>name:  ${this.name}</li>
                <li>price:  ${this.price}</li>
            </ul>`);
    };
}


class BookErr extends Error{
    constructor(book) {
        super(`som wrong with book: ${book}`);
        this.name = "Book Error"
    }
}


books.forEach((elem) => {
    try {
        if (elem.author && elem.price) {
            new RenderBook(elem.author, elem.name, elem.price).render(bookContainer);
        }else {
            throw new BookErr(elem.name);
        }
    }catch (err){
        if(err.name === "Book Error"){
            console.warn(err);
        }else {
            throw err;
        }
    }
});